% !TEX encoding = UTF-8 Unicode

\documentclass[a4paper]{article}

\usepackage{color}
\usepackage{url}
\usepackage[T2A]{fontenc} 
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{float}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{lipsum}
\usepackage{mwe}
\usepackage{multirow}

\usepackage[english,serbian]{babel}
\usepackage[unicode]{hyperref}
\hypersetup{colorlinks,citecolor=red,filecolor=green,linkcolor=blue,urlcolor=blue}

\begin{document}

\title{Minimalna sirina stabla\\ \small{Seminarski rad u okviru kursa\\Računaska inteligencija\\ Matematički fakultet}}

\author{Nemanja Lisinac\\ nemanjalisinac@gmail.com}
\date{25.~septembar 2022.}
\maketitle


\tableofcontents

\newpage

\section{Uvod}
\label{sec:uvod}
\subsection{Stablo}
U teoriji grafova, stablo je graf  u kome su bilo koja dva čvora povezana tačno jednom granom. Odnosno, svaki povezan graf bez ciklusa je stablo. \cite{treeGraphTheory}\\

\textbf{Definicija 1}:
Stablo je neorijentisan prost graf {\em G} koji zadovoljava bilo koji od sledećih uslova koji su ekvivalentni:
\begin{itemize}
\item {\em G} je povezan i nema prostih ciklusa
\item {\em G} nema prostih ciklusa, a prost ciklus se dobija ako se bilo koja nova grana doda u {\em G}
\item {\em G} je povezan, ali ako se ukloni bilo koja grana iz {\em G}, više neće biti povezan
\item Bilo koja dva čvora u {\em G} su povezana jedinstvenom prostom granom.\\
\end{itemize}
A ako {\em G} ima konačno mnogo čvorova ({\em n})  onda važi:
\begin{itemize}
\item {\em G} je povezan i ima {\em n} - 1 grana
\item {\em G} nema prostih ciklusa i ima {\em n} - 1 grana.
\end{itemize}

\begin{figure}[h!]
	\begin{center}
		\includegraphics[scale=0.4]{stablo.jpg}
	\end{center}
	\caption{Stablo sa 6 čvorova i 5 grana}
\end{figure}
\newpage
\subsection{Razlaganje stabla}
\textbf{Definicija 2}:
Ako je {\em G= (V, E)} graf, 
stablo razlaganja je uređeni par 
{\em (X, T)} gde je {\em T= (I, F)} stablo, 
X = $( \,	X_1, X_2 ... X_n	) \,$ 
 je familija podskupova {\em V} tako da vazi:
\begin{itemize}
\item $\cup_{i \in I} X_i = V$
\item $\forall  (v, w )  \in  E$,  $\exists i \in I$ t.d $u, v \in X_i$ (v,w) je ivica,
\item $\forall v \in V$, skup ${i \in I : v \in X_i}$ je povezano podstablo od {\em T}.\cite{algorithmicAspects}\\
\end{itemize}

\begin{figure}[h!]
	\begin{center}
		\includegraphics[scale=0.7]{razlaganje.jpg}
	\end{center}
	\caption{Graf sa osam čvorova, i njegovo razlaganje na stablo. \cite{razlaganje}}
\end{figure} 


\section{Opis problema}
\label{sec:opis}
\textbf{Definicija 3}:\\
\textbf {Širina stabla} $(|X_i - 1|)$ je kardinalnost njegovog najvećeg podskupa datog stabla razlaganja umanjena za 1. \textbf{Minimalna širina stabla} $tw(G)$ je najmanja sirina koja se može postići  razlaganjem stabla {\em G}.\cite{compendium}\\
\\Ako pretpostavimo da je razlaganje na slici 2 najbolje (najmanje širine), onda je $tw(G)$ = 2.

Tako definisan, problem minimalne širine proizvoljnog stabla spada u NP kompletne probleme i u ovom radu ćemo razmatrati različite algoritme koji određuju minimalnu širinu datog stabla.\newpage

\section{$O(4^n)$ Rekurzivni algoritam}
\label{sec:rekurzivni}
Kako bi razmotrili rekurzivni algoritam, uvešćemo jos jednu definiciju:\\\\
\textbf{Definicija 4}:\\
Ako je {\em G= (V, E)} graf,  $S \subseteq V$ i čvor $v \in V - S$:\\
$Q_G (S, v) = \{\,w \in V - S - \{w\} \mid \,\}$ takav da postoji put od $v$ do $w$ u G[$S \cup \{v, w\}$]\\\\
Izračunavanje $Q_G (S, v)$ se može izvršiti u vremenu O(n + m) tako što se za svako $w \in V - S - \{v\}$ proveri da li $w$ ima suseda u G[$S \cup \{v\}$] koji sadži $v$. \cite{exactAlgorithms}\\\\
Pseudokod rekurzivnog algoritma razmotrenog u radu {\em On exact algorithms for treewidth: Hans L. Bodlaender} \cite{exactAlgorithms}
\begin{verbatim}
ALGORITHM : Recursive-Treewidth(Graph G, Vertex Set L, Vertex Set S)
if |S| = 1 then
    Suppose S = {v}.
    return Q(L, v)
end if

Set Opt = infinity.

for all sets S' in S, |S'| = |S|/2 do
    Compute v1 = Recursive-Treewidth(G, L, S')
    Compute v2 = Recursive-Treewidth(G, L union S', S - S');
    Set Opt = min {Opt, max {v1, v2}};
end for

return Opt
\end{verbatim}
Prostorna složenost algoritma je jednaka dubini rekurzije O(log n). Dok je vremenska složenost algoritma $O(4^n)$.

\newpage
\section{$O(2^n)$ DP algoritam}
\label{sec:dpAlgoritam}
Dinamičko programiranje je tehnika optimizacije kojom pokušavamo da optimizujemo izvršavanje algoritma tako što čuvamo vrednosti izvršavanja nekih skupih operacija i time izbegavamo njihovo ponovno izračunavanje. Ovom tehnikom optimizacije je moguće svesti neke eksponencijalne probleme na polinomijalne.\\
Na primer, rekurzivno izračunavanje Fibonačijevih brojeva je eksponencijalne vremenske složenosti i DP optimizacijom (čuvanjem konačnih rešenja prethodnih koraka), smanjujemo vremensku složenost na linearnu.\cite{dp}\\ 

Primenom DP-a, možemo smanjiti vremensku složenost prethodnog algoritma sa $O(4^n)$ na $O(2^n)$.


\begin{verbatim}
ALGORITHM : Dynamic-Programming-Treewidth(Graph G = (V, E))

Set T W(X) = -infinity.

for i = 1 to n do
    for all sets S in V with |S| = i do
         Set TW(S) = min (for v in S) max T W(S - {v}), |Q(S - {v}, v)|
    end for
end for

return TW(V)
\end{verbatim}
Prostorna složenost algoritma je jednaka sa vremenskom i iznosi $O(2^n)$.\\\\

\newpage
\section{Optimizacione metode}
\label{sec:optimizacioniMetodi}


\textbf{Opšta ideja računjanja širine}:\\
\begin{itemize}
\item Na datom grafu izaberemo početni čvor (čvor koji se eliminiše).
\item Trenutna minimalna širina je: tw = max(neigh(čvor), 0).
\item Spojimo međusobno sve susede datog čvora ivicom.
\item Eliminišemo odabrani čvor i obrišemo sve ivice u kojima se on nalazi.
\item Ponavljamo dok nam ne ostane samo jedan čvor. U svakoj sledećoj iteraciji, minimalna širina se računa kao tw = max(neigh(čvor), tw).
\end{itemize}
Možemo primetiti da nam širina direkno zavisi od redosleda odabira čvorova kojim ćemo eliminisati. Dok je minimalna širina je najmanja širina pri svim mogućim obilascima.

\begin{figure}[h!]
	\centering
	\begin{minipage}{0.33\textwidth}
		\centering
		\includegraphics[width=\textwidth]{p1.jpg}
	\end{minipage}\hfill
	\begin{minipage}{0.33\textwidth}
		\centering
		\includegraphics[width=\textwidth]{p2.jpg}
	\end{minipage}
	\begin{minipage}{0.33\textwidth}
		\centering
		\includegraphics[width=\textwidth]{p3.jpg}
	\end{minipage}
	\caption{Proces eliminacije}
\end{figure}

\subsection{Branch and bound}
Branch and bound je optimizaciona metoda koja ima se koristi u cilju rešavanja kombinatornih i diskretnih optimizacionih problema. Sastoji se iz dve funkcije branch i bound. Branch funkcija služi kako bi se dodao čvor u stablo pretrage, dok funkcija bound koristi svojstva stabla kako bi smanjila stablo pretrage. Tako da na branch možemo gledati kao na neki bruteforce deo, dok je bound pametan deo koji nam znatno smanjuje stablo pretrage. I kvalitet algoritma zavisi najvise od tog bound dela.\\

Prilikom odabira branch funkcije, korišćena su dva pristupa DFS i BFS obilazak stabla. BFS obilazak je sporiji, ali garantuje minimalnost rešenja, dok je DFS obilazak znatno brži ali ne garantuje da će rešenje biti minimalno (može se napraviti varijacija DFS pristupa gde se obilaze sve moguće varijante ali se onda mora paralelizovati kako ne bi bilo iste brzine kao BFS). 

Sve do sada posećene čvorove, zajedno sa redosledom eliminacije i širinom u datom trenutku ćemo čuvati u hash mapi posećenih čvorova. Unutar bound fukcije ćemo imati pristup toj mapi kako bi izbegli dupla stanja. U slučaju duplih stanja, treba videti da li se čvor odbacuje ili se dodaje u stablo pretrage. Tu ćemo iskoristiti činjenicu da, ako imamo graf {\em G= (V, E)} sa čvorovima 1,2,3, ako u prvom koraku eliminišemo čvor 1, rezultat izvršavanja algoritma će biti isti za bilo koji preostali odabir tw(2 ->3) == tw (3 ->2).

\begin{verbatim}
Pseudokod :
class Node (vertex, graph, level, treewidth)

Main():
map = {}
Q = []
Q.put(Node(0, graph, [], 0)

while !Q.empty():
    branch(Q.pop())

return map[`treewidth']

branch(node):
      minTreewidth = max( node.treewidth, neighbors(node))
if (bound(node) == true):
      expand(node)
      end if

bound(node):
     if (level = n-1):
     globalTreewidth = min(globalTreewidth, node.treewidth)
     end if
     
     if map[node] doesn't exist:
        map[node] = node.treewidth
        return true
        end if
     else:
     if map[node] > node.treewidth
        map[node] = node.treewidth
        return true
        end if
     end else
     return false

\end{verbatim}

\newpage
\subsection{Genetski algoritam}
Genetski algoritam je nastao po uzoru na Darvinovu teoriju evolucije. Na početku problema definišemo kriterijume zaustavljanja, veličinu populacije i definišemo jedinku. Svaka jedinka se treba reprezentovati tako da nam predstavlja rešenje problema. Takođe, sem populacije i jedinke moramo definisati operatore: fitness funkcije, selekcije, operatora mutacije, operatora ukrštanja.\\
Fitnes funkcija nam označava koliko je naše rešenje dobro, selekcija nam označava način odabira jedinki za ukrštanje (reprodukciju) najčešće su turnirska ili ruletska.
Operator mutacije nam određuje mogućnost da se promeni jedan gen u jedinki (sa malom verovatnoćom), dok je najbitniji operator ukrštanja koji nam određuje kakvi će se potomci dobiti ukrštanjem dva roditelja.

Pri rešavanju našeg problema, korišćena su turnirska selekcija, verovatnoća mutacije od 5\% kao i elitizam (jedinke koje su najbolje će se čuvati pri sledećim generacijama).

\begin{verbatim}
Pseudokod:

START
Genate population
Calculate fitness
while generations < limit:
    selection
    crossover
    mutation
    calculate fitness
STOP
\end{verbatim}

\newpage
\section{Eksperimentalni rezultati}
\label{sec:eksperiment}

Algoritmi su testirani na desktop racunaru sledecih specifikacija:
\begin{itemize}
\item {\em CPU:} Ryzen 5 3600
\item {\em RAM:} 16 GB
\item {\em OS:} Windows 10 64-bit
\end{itemize}

Za testiranje su korišćeni manji grafovi u DIMACS formatu,  autogenerisani grafovi i pojedini benchmark grafovi. Svaki od grafova se nalazi u \href{https://gitlab.com/lisk0/ri-seminarski/-/tree/main/graphs}{git repozitorijumu}.

\subsection{Rekurzivni algoritam}
Algoritam je implementiran korišćenjem eksterne biblioteke za reprezentaciju grafova \em{igraph} u programskom jeziku \em{Python} i ličnog modula - {\em graphutils.py} koji kombinuje biblioteku sa drugim modulima u cilju učitavanja i automatskog generisanja grafova. Mana algoritma je u tome što se zbog velikog broja rekurzivnih poziva, čak i pri manjem broju čvorova, algoritam mnogo sporije izvršava od DP algoritma. \\

\subsection{DP algoritam}
Reprezentacija grafova je urađena na isti način kao i kod rekurzivnog algoritma. Ideja je čuvati sve prethodne korake izračunavanja u hash mapi i time smanjiti vremensku složenost rekurzivnog algoritma.\\ 

\subsection {Branch and bound algoritam}
Korišćeni su i BFS kao i DFS (greedy) obilazak stabla. U slučaju DFS obilaska, algoritam nam aproksimira gornju granicu širine, dok se u slučaju BFS dobija tačna vrednost za širinu. Jedan čvor u stablu pretrage se sastoji od odabranog čvora za eliminaciju, grafa iz kog ćemo eliminisati odabrani čvor, prethodno eliminisanih čvorova, i vrednosti tačne ili aproksimirane širine u slučaju DFS eliminisanog grafa. 

\subsection{Genetski algoritam}
Svaka jedinka se sastoji iz redosleda eliminacije čvorova i početnog grafa. Korišćena je veličina populacije 20, broj generacija 5, jednopoziciono ukrštanje koje je definisano na sledeći način:
Nasumice je odabrana pozicija ukštanja, dete1 će naslediti gene roditelja1 do mesta ukrštanja, ostatak će biti proizvoljno popunjen tako da geni predstavljaju potpun obilazak, dok će kod deteta2 nakon mesta ukrštanja biti geni roditelja2 dok će ostatak biti popunjen analogno.

\subsection{Autogenerisani grafovi}
U modulu {\em graphutils.py} je definisana funkcija koja čita grafove iz fajlova, kao i funkcije prikaza grafa, snimanja u obliku pogodnom za korišćenje od strane biblioteke. Funkcija autogeneracije prima broj čvorova grafa i kao rezultat vraća neusmeren graf koji nastaje nasumičnim popunjavanjem matrice susedstva. Generisani grafovi imaju od 10 do 16 čvorova.
\begin{figure}[h!]
	\centering
	\begin{minipage}{0.33\textwidth}
		\centering
		\includegraphics[width=\textwidth]{random10.jpg}
	\end{minipage}\hfill
	\begin{minipage}{0.33\textwidth}
		\centering
		\includegraphics[width=\textwidth]{random12.jpg}
	\end{minipage}
	\begin{minipage}{0.33\textwidth}
		\centering
		\includegraphics[width=\textwidth]{random16.jpg}
	\end{minipage}
	\caption{Autogenerisani grafovi sa 10, 12 i 16 čvorova}
\end{figure}
\newpage

\begin{center}
\begin{table}[h!]
\begin{tabular}{|lcccc|}
\hline
\multicolumn{5}{|c|}{\textbf{Rezultat algoritma}}                                                                                                                                   \\ \hline
\multicolumn{1}{|c|}{\textbf{}}     & \multicolumn{2}{c|}{rekurzivni}                                       & \multicolumn{2}{c|}{dinamički}                                        \\ \hline
\multicolumn{1}{|c|}{\textbf{graf}} & \multicolumn{1}{l|}{rešenje} & \multicolumn{1}{l|}{vreme izvršavanja} & \multicolumn{1}{l|}{rešenje} & \multicolumn{1}{l|}{vreme izvršavanja} \\ \hline
\multicolumn{1}{|l|}{testRyan}      & \multicolumn{1}{c|}{2}       & \multicolumn{1}{c|}{0.0140 s}          & \multicolumn{1}{c|}{2}       & 0.0020 s                               \\ \hline
\multicolumn{1}{|l|}{random11}      & \multicolumn{1}{c|}{7}       & \multicolumn{1}{c|}{1.6263 s}          & \multicolumn{1}{c|}{7}       & 0.0520 s                               \\ \hline
\multicolumn{1}{|l|}{random12}      & \multicolumn{1}{c|}{6}       & \multicolumn{1}{c|}{5.2461 s}          & \multicolumn{1}{c|}{6}       & 0.1220 s                               \\ \hline
\multicolumn{1}{|l|}{random13}      & \multicolumn{1}{c|}{8}       & \multicolumn{1}{c|}{26.4259 s}         & \multicolumn{1}{c|}{8}       & 0.3100 s                               \\ \hline
\multicolumn{1}{|l|}{random14}      & \multicolumn{1}{c|}{7}       & \multicolumn{1}{c|}{79.3908 s}         & \multicolumn{1}{c|}{7}       & 0.6401 s                               \\ \hline
\multicolumn{1}{|l|}{random15}      & \multicolumn{1}{c|}{7}       & \multicolumn{1}{c|}{358.8417 s}        & \multicolumn{1}{c|}{7}       & 1.6193 s                               \\ \hline
\multicolumn{1}{|l|}{random16}      & \multicolumn{1}{c|}{10}        & \multicolumn{1}{c|}{1277.8297 s}                  & \multicolumn{1}{c|}{10}        &  4.0579 s                                      \\ \hline
\end{tabular}
\end{table}
\end{center}

\begin{figure}[h]
	\begin{center}
		\includegraphics[scale=0.4]{grafikon.png}
	\end{center}
\end{figure} 

\begin{table}[]
\begin{tabular}{|lcccc|}
\hline
\multicolumn{5}{|c|}{\textbf{Rezultat algoritma Branch and bound}}                                                                                                                  \\ \hline
\multicolumn{1}{|c|}{\textbf{}}     & \multicolumn{2}{c|}{BFS}                                              & \multicolumn{2}{c|}{DFS aproksimacija}                                \\ \hline
\multicolumn{1}{|c|}{\textbf{graf}} & \multicolumn{1}{l|}{rešenje} & \multicolumn{1}{l|}{vreme izvršavanja} & \multicolumn{1}{l|}{rešenje} & \multicolumn{1}{l|}{vreme izvršavanja} \\ \hline
\multicolumn{1}{|l|}{testRyan}      & \multicolumn{1}{c|}{2}       & \multicolumn{1}{c|}{0.007 s}           & \multicolumn{1}{c|}{2}       & 0.0020 s                               \\ \hline
\multicolumn{1}{|l|}{random11}      & \multicolumn{1}{c|}{7}       & \multicolumn{1}{c|}{0.963 s}           & \multicolumn{1}{c|}{7}       & 0.0060 s                               \\ \hline
\multicolumn{1}{|l|}{random12}      & \multicolumn{1}{c|}{6}       & \multicolumn{1}{c|}{3.225 s}           & \multicolumn{1}{c|}{8}       & 0.1002 s                               \\ \hline
\multicolumn{1}{|l|}{random13}      & \multicolumn{1}{c|}{8}       & \multicolumn{1}{c|}{11.665 s}          & \multicolumn{1}{c|}{10}      & 0.013 s                                \\ \hline
\multicolumn{1}{|l|}{random14}      & \multicolumn{1}{c|}{7}       & \multicolumn{1}{c|}{15.904 s}          & \multicolumn{1}{c|}{8}       & 0.015 s                                \\ \hline
\multicolumn{1}{|l|}{random15}      & \multicolumn{1}{c|}{10}      & \multicolumn{1}{c|}{168.001 s}         & \multicolumn{1}{c|}{11}      & 0.030 s                                \\ \hline
\multicolumn{1}{|l|}{random16}      & \multicolumn{1}{c|}{10}      & \multicolumn{1}{c|}{194.449 s}         & \multicolumn{1}{c|}{10}      & 0.031 s                                \\ \hline
\end{tabular}
\end{table}

\begin{table}[]
\begin{tabular}{|lcccc|}
\hline
\multicolumn{5}{|c|}{\textbf{Rezultat Genetskog algoritma}}                                                                                                                              \\ \hline
\multicolumn{1}{|c|}{\textbf{}}          & \multicolumn{2}{c|}{N=20, Generations=5}                              & \multicolumn{2}{c|}{N=40, Generations=8}                              \\ \hline
\multicolumn{1}{|c|}{\textbf{graf}}      & \multicolumn{1}{l|}{rešenje} & \multicolumn{1}{l|}{vreme izvršavanja} & \multicolumn{1}{l|}{rešenje} & \multicolumn{1}{l|}{vreme izvršavanja} \\ \hline
\multicolumn{1}{|l|}{testRyan}           & \multicolumn{1}{c|}{2}       & \multicolumn{1}{c|}{0.012 s}           & \multicolumn{1}{c|}{2}       & 0.019 s                                \\ \hline
\multicolumn{1}{|l|}{random14}           & \multicolumn{1}{c|}{7}       & \multicolumn{1}{c|}{0.105 s}           & \multicolumn{1}{c|}{7}       & 0.305 s                                \\ \hline
\multicolumn{1}{|l|}{random15}           & \multicolumn{1}{c|}{11}      & \multicolumn{1}{c|}{0.188 s}           & \multicolumn{1}{c|}{10}      & 0.536 s                                \\ \hline
\multicolumn{1}{|l|}{random19}           & \multicolumn{1}{c|}{12}      & \multicolumn{1}{c|}{0.292 s}           & \multicolumn{1}{c|}{13}      & 0.828 s                                \\ \hline
\multicolumn{1}{|l|}{\textbf{queen7\_7}} & \multicolumn{1}{c|}{36}      & \multicolumn{1}{c|}{2.515 s}           & \multicolumn{1}{c|}{36}      & 7.669 s                                \\ \hline
\multicolumn{1}{|l|}{\textbf{eli51}}     & \multicolumn{1}{c|}{6}       & \multicolumn{1}{c|}{0.747 s}           & \multicolumn{1}{c|}{6}       & 2.288 s                                \\ \hline
\multicolumn{1}{|l|}{\textbf{miles500}}  & \multicolumn{1}{c|}{30}      & \multicolumn{1}{c|}{15.35 s}           & \multicolumn{1}{c|}{30}      & 48.392 s                               \\ \hline
\end{tabular}
\end{table}
\newpage
\section{Zaključak}
Vidimo da za male instance i rekurzivni i dp algoritam daju tačna rešenja. Vreme izvršavanja rekurzivnog algoritma značajno raste sa povećanjem broja čvorova. Shodno vremenima, možemo zaključiti da je razlika u brzini ova dva algoritma značajna, pa je samim tim DP algoritam bolji i ako ima veću memorijsku složenost. Veliki problem predstavljenih algoritama čini nemogućnost paralelizacije u datom obliku, pa bi dalji tok unapređivanja uključivao paralelizaciju što bi znacajno ubrzalo izvršavanje.\\
Postoje modifikacije rekurzivnog algoritma koje mogu smanjiti vremensku složenost rekurzivnog algoritma (do $O(2^n)$).\cite{exactAlgorithms}\\
Takođe, postoje i algoritmi aproksimacije i algoritmi koji se baziraju na triangulaciji koji daju rešenje problema minimalne širine stabla. 

Što se tice Branch and bound algoritma, BFS verzija koja daje egzaktne rezultate se pokazala bolje od rekurzivnog, ali gore od DP algoritma, dok se DFS (greedy) verzija može koristiti kao osnova za određivanje gornje granice širine pri nekim algoritmima optimizacije zbog svoje brzine.

Genetski algoritam daje najbolje rezultate i vreme izvršavanja zavisi najviše od veličine populacije i broja generacija i ne raste eksponencijalno sa širinom grafa. U tabeli možemo videti rezultate izvršavanja genetskog algoritma na benchmark algoritmima u zagradi su tačne vrednosti queen7 (tw=35), eli51 (tw=6) i miles500 (tw=22) nad kojima nije moguće dobiti rezultate primenom drugih algoritama. Kod grafa miles500, najbolji rezultat tw=28 je dobijen uz N=40, Generations = 7.

\newpage
\addcontentsline{toc}{section}{Literatura}
\appendix

\iffalse
\bibliography{reference} 
\bibliographystyle{plain}
\fi

\begin{thebibliography}{6}

\bibitem{treeGraphTheory} {Bender, Edward A.; Williamson, S. Gill (2010),  \emph{Lists, Decisions and Graphs. With an Introduction to Probability} }
\bibitem{algorithmicAspects}{Robertson, N., Seymour, P.D.: \emph{Graph minors. II: Algorithmic aspects of treewidth. J. Algorithms 7, 309-322 (1986)}}
\bibitem{razlaganje} {slika 2, Razlaganje stabla, Wikipedia}
\bibitem{compendium} \href{https://www.csc.kth.se/~viggo/wwwcompendium/node67.html}{Viggo Kann (2000), \emph{Compendium}}
\bibitem{exactAlgorithms} \href{https://folk.uib.no/nmiff/articles/2012/2012k.pdf} {Hans L. Bodlaender, Fedor V. Fomin, Arie M. C. A. Koster, Dieter Kratsch
, Dimitrios M. Thilikos (2006),  \emph{On exact algorithms for treewidth} }
\bibitem{dp}{Dimitri P. Bertsekas: \emph{Dynamic Programming and Optimal Control, Vol. 1} }

\end{thebibliography}

\end{document}