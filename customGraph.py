class CustomGraph:
    def __init__(self, vertices, edgelist):
        self.vertices = vertices
        tm = sorted(list(edgelist))
        self.edgelist = []
        for (x,y) in tm:
            if (x == y):
                continue
            edge = (x, y)
            self.edgelist.append(edge)
            
    def neighbors(self, v):
        l = list()
        for (x, y) in self.edgelist:
            if (x == v):
                l.append(y)
            if (y == v):
                l.append(x)
        return l
    
    def get_edgelist(self):
        return self.edgelist
    
    def add_edges(self, edges):
        tmp = set(self.edgelist)
        for (x,y) in edges:
            if (x == y):
                continue
            edge = (x,y)
            if (x > y):
                edge = (y,x)
            tmp.add(edge)
        lst = sorted(list(tmp))
        self.edgelist = lst
        
    def get_adjlist(self):
        k = {}
        for i in range(self.vertices):
            neig = self.neighbors(i)
            k[i] = neig
        return k

def getGraphFromDIMACS(file):
    f= open(file, "r")
    n_vertices = 0
    edges = []
    total_edges = 0
    for line in f:
        line = line.replace("\n", "")
        splited = line.split(" ")
        if (splited[0] == "c"):
            continue
        else:
            if (splited[0] == "p"):
                n_vertices = int(splited[2])
                total_edges = int(splited[3])
            else:
                edges.append((int(splited[1]) - 1, int(splited[2]) - 1))
    graph = CustomGraph(n_vertices, edges)
    return graph

def graphWithoutVertex(v, g):
        import itertools
        import copy
        
        graph = copy.deepcopy(g)
        neighbors = graph.neighbors(v)
        connected = list(itertools.combinations(neighbors, 2))
        edgelist = graph.get_edgelist()
        tmp = copy.deepcopy(edgelist)
        graph.add_edges(connected)
        
        for (x,y) in edgelist:
            if (x == v or y == v):
                tmp.remove((x,y))
        
        return CustomGraph(graph.vertices, list(set(tmp)))
    
def loadFromGML(file):
    import igraph
    tempGraph = igraph.load(file)
    return CustomGraph(tempGraph.vcount(), tempGraph.get_edgelist())