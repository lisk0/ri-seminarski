from igraph import Graph, plot
import matplotlib.pyplot as plt
import numpy as np
from itertools import combinations
import copy

def getGraphFromDIMACS(file):
    f= open(file, "r")
    n_vertices = 0
    edges = []
    total_edges = 0
    for line in f:
        line = line.replace("\n", "")
        splited = line.split(" ")
        if (splited[0] == "c"):
            continue
        else:
            if (splited[0] == "p"):
                n_vertices = int(splited[2])
                total_edges = int(splited[3])
            else:
                edges.append((int(splited[1]) - 1, int(splited[2]) - 1))
    graph = Graph(n_vertices, edges)
    return graph

def getGraphFromDIMACS0(file):
    f= open(file, "r")
    n_vertices = 0
    edges = []
    total_edges = 0
    for line in f:
        line = line.replace("\n", "")
        splited = line.split(" ")
        if (splited[0] == "c"):
            continue
        else:
            if (splited[0] == "p"):
                n_vertices = int(splited[2])
                total_edges = int(splited[3])
            else:
                edges.append((int(splited[1]), int(splited[2])))
    graph = Graph(n_vertices, edges)
    return graph


def generateAndSaveImage(graph, imageName):
    fig, ax = plt.subplots(figsize=(5,5))
    plot(
    graph,
        target=ax,
        layout="circle",
        vertex_size=0.1,
        vertex_frame_width=1.0,
        vertex_frame_color="white",
        vertex_label_size=1.0,
    )

    plt.show()
    fig.savefig(f"{imageName}.jpg")
    graph.write_gml(f"{imageName}.gml")
    
def getRandomGraph(n_vertices):
    adjacency_matrix = np.random.randint(0,2,(n_vertices,n_vertices))
    graph = Graph.Adjacency(adjacency_matrix)
    return graph.as_undirected()

def graphWithoutVertex(v, graph):
    neighbors = graph.neighbors(v)
    connected = list(combinations(neighbors, 2))
    edgelist = graph.get_edgelist()
    tmp = copy.deepcopy(edgelist)
    for (x,y) in edgelist:
        if (x == v or y == v):
            tmp.remove((x,y))
    graph.add_edges(connected)
    return Graph(list(set(tmp)))